﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
    [Header("Source")]
    public AudioSource soundSource;

    [Header("Clip")]
    public AudioClip focus;
    public AudioClip jump;
    public AudioClip openDoor;
    public AudioClip pickJewel;
    public AudioClip select;

    public void PlayJump()
    {
        soundSource.clip = jump;
        soundSource.Play();
    }
    public void PlayOpenDoor()
    {
        soundSource.clip = openDoor;
        soundSource.Play();
    }
    public void PlayPickJewel()
    {
        soundSource.clip = pickJewel;
        soundSource.Play();
    }
    public void PlaySelect()
    {
        soundSource.clip = select;
        soundSource.Play();
    }
    public void PlayFocus()
    {
        soundSource.clip = focus;
        soundSource.Play();
    }
}

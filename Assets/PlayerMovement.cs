﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class PlayerMovement : MonoBehaviour
{
    [Header("Controller")]
    private GameControls playerControls;

    [Header("Component")]
    private Rigidbody2D rigidbody;

    [Header("Private Data")]
    private int JewelsToRetrieve;
    private bool doorClosed;

    [Header("Public Data")]
    public GameObject Jewels;
    public GameObject Door;

    [Header("Player Movement Data")]
    public float movementSpeed;
    public float jumpForce;

    [Header("Audio")]
    private GameObject Audio;

    [Header("Animation")]
    private Animator Animator;
    public  bool grounded;

    private void Awake()
    {
        playerControls = new GameControls();
        playerControls.Player.Jump.performed += _ => Jump();
        playerControls.Player.Action.performed += _ => Action();

        rigidbody = GetComponent<Rigidbody2D>();

        Jewels = GameObject.Find("Jewels");
        Door = GameObject.Find("Door");
        Audio = GameObject.Find("Audio");
        Animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        playerControls.Enable();
    }
    
    private void OnDisable()
    {
        playerControls.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        JewelsToRetrieve =  Jewels.transform.childCount;
        // Player Data
        movementSpeed = 3;
        jumpForce = 8;
        doorClosed = true;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void FixedUpdate()
    {
        Movement();
        if (JewelsToRetrieve <= 0 && doorClosed)
        {
            Invoke("OpenDoor", 1f);
            doorClosed = false;
        }
        if(rigidbody.velocity.y > -0.01 && rigidbody.velocity.y < 0.01)
        {
            Debug.Log("Velocity - 0");
            if (grounded)
            {
                Animator.SetBool("Jump", false);
            }
            else
            {
                Animator.SetBool("Grip", true);
            }
        }else
        {
            Animator.SetBool("Grip", false);
        }
    }

    void OpenDoor()
    {
        Audio.GetComponent<SoundScript>().PlayOpenDoor();
        Door.GetComponent<Door>().Open();
    }

    void Jump()
    {
        if (Mathf.Abs(rigidbody.velocity.y) < 0.001f)
        {
            DoJump();
        }
    }

    public void DoJump()
    {
        Animator.SetBool("Jump", true);
        Audio.GetComponent<SoundScript>().PlayJump();
        rigidbody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
    }

    void Action()
    {

    }

    void Movement()
    {
        float xMovement = playerControls.Player.xMovement.ReadValue<float>();

        transform.position += new Vector3(xMovement * movementSpeed, 0, 0) * Time.deltaTime;
        if(xMovement > 0.01 || xMovement < -0.01)
        {
            Animator.SetBool("Walk", true);
        }else
        {
            Animator.SetBool("Walk", false);
        }
        if(xMovement > 0.01)
        {
            transform.localScale = new Vector3(1,1,1);
        }else if (xMovement < -0.01)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "End")
        {
            LevelSelector.Level++;
            if (LevelSelector.Level >= SceneManager.sceneCountInBuildSettings)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                SceneManager.LoadScene(LevelSelector.Level);
            }
            DontDestroyOnLoad(Audio);
        }
        else if (collider.name == "Ground")
        {
            grounded = true;
        }else 
        {
            if (!collider.GetComponent<Jewel>().isDeactivate)
            {
                Audio.GetComponent<SoundScript>().PlayPickJewel();
                collider.GetComponent<Jewel>().Deactivate();
                JewelsToRetrieve--;
            }
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.name == "Ground")
        {
            grounded = false;
        }
    }
}
//float xMovement = playerControls.Theodore.XMovement.ReadValue<float>();
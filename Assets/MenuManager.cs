﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [Header("Controller")]
    private GameControls playerControls;

    [Header("Audio")]
    public GameObject Audio;

    [Header("Buttons")]
    public Image playButton;
    public Image quitButton;

    public Sprite basicSprite;
    public Sprite selectSprite;

    [Header("Private data")]
    private int selected;

    private void Awake()
    {
        playerControls = new GameControls();
        playerControls.Menu.Up.performed += _ => Up();
        playerControls.Menu.Down.performed += _ => Down();
        playerControls.Menu.Select.performed += _ => Select();
        selected = 1;
    }

    private void OnEnable()
    {
        playerControls.Enable();
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        LevelSelector.Level = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LaunchGame()
    {

        Audio.GetComponent<SoundScript>().PlaySelect();
        SceneManager.LoadScene(LevelSelector.Level);
        DontDestroyOnLoad(Audio);
    }

    public void QuitGame()
    {
        Audio.GetComponent<SoundScript>().PlaySelect();
        Application.Quit();
    }

    void Up()
    {
        Audio.GetComponent<SoundScript>().PlayFocus();
        selected = -selected;
        SwitchSprite();
    }

    void Down()
    {
        Audio.GetComponent<SoundScript>().PlayFocus();
        selected = -selected;
        SwitchSprite();
    }


    void SwitchSprite()
    {
        if(playButton.sprite == selectSprite)
        {
            playButton.sprite = basicSprite;
            quitButton.sprite = selectSprite;
        }else
        {
            playButton.sprite = selectSprite;
            quitButton.sprite = basicSprite;
        }
    }

    void Select()
    {
        if(selected == 1)
        {
            LaunchGame();
        }else if (selected == -1)
        {
            QuitGame();
        }
    }
}

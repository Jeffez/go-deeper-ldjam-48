﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Jewel : MonoBehaviour
{
    public bool isDeactivate;
    public void Deactivate()
    {
        Color hideColor = new Color(1, 1, 1, 0);
        StartCoroutine(LerpColor(transform.GetChild(0).gameObject, hideColor, 1f));
        isDeactivate = true;
    }

    IEnumerator LerpColor(GameObject go, Color endValue, float duration)
    {
        float time = 0;
        Color startValue = go.GetComponent<Tilemap>().color;

        while (time < duration)
        {
            go.GetComponent<Tilemap>().color = Color.Lerp(startValue, endValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        go.GetComponent<Tilemap>().color = endValue;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour
{
    [Header("Source")]
    public AudioSource drumSource;
    public AudioSource leadSource;
    public AudioSource leadHSource;
    public AudioSource leadBSource;
    public AudioSource secondarySource;
    public AudioSource secondaryHSource;
    public AudioSource secondaryBSource;


    [Header("Clip")]
    public AudioClip drum;
    public AudioClip mtv1;
    public AudioClip mtv1h;
    public AudioClip mtv1b;
    public AudioClip mtv2;
    public AudioClip mtv2h;
    public AudioClip mtv2b;


    // Start is called before the first frame update
    void Start()
    {
        drumSource.clip = drum;
        drumSource.Play();

        Invoke("PlayRandomMotive", 3);
    }


    void PlayMotive1()
    {
        Debug.Log(leadSource.clip);
        if(leadSource.clip == null)
        {
            Motive1(leadSource,leadHSource, leadBSource);
        }else
        {
            Motive1(secondarySource, secondaryHSource, secondaryBSource);
        }
    }

    void Motive1(AudioSource source, AudioSource sourceH, AudioSource sourceB)
    {
        StartCoroutine(IntroMotive1(source, 0));
        StartCoroutine(CoreMotive1(source, sourceH, sourceB, 24));
    }

    IEnumerator IntroMotive1(AudioSource source, float wait)
    {
        yield return new WaitForSeconds(wait);
        StartCoroutine(Play(source, mtv1, 0));
    }

    IEnumerator CoreMotive1(AudioSource source, AudioSource sourceH, AudioSource sourceB, float wait)
    {
        yield return new WaitForSeconds(wait);
        StartCoroutine(Play(source, mtv1, 0));
        StartCoroutine(Play(sourceH, mtv1h, 0));
        StartCoroutine(Play(sourceB, mtv1b, 0));

        float rand = Random.Range(0f, 100f)/100;

        Debug.Log(rand);
        if (rand > 0.5f)
        {
            Invoke("PlayRandomMotive", 0);
            StartCoroutine(CleanSource(source, 24));
            StartCoroutine(CleanSource(sourceH, 24));
            StartCoroutine(CleanSource(sourceB, 24));
        }
        else
        {
            StartCoroutine(CoreMotive1(source, sourceH, sourceB, 24));
        }
    }
    void PlayMotive2()
    {
        if (leadSource.clip == null)
        {
            Motive2(leadSource, leadHSource, leadBSource);
        }
        else
        {
            Motive2(secondarySource, secondaryHSource, secondaryBSource);
        }
    }

    void Motive2(AudioSource source, AudioSource sourceH, AudioSource sourceB)
    {
        StartCoroutine(IntroMotive2(sourceB, source, 0));
        StartCoroutine(CoreMotive2(source, sourceH, sourceB, 24));
    }

    IEnumerator IntroMotive2(AudioSource sourceb, AudioSource source, float wait)
    {
        yield return new WaitForSeconds(wait);
        StartCoroutine(Play(sourceb, mtv2b, 0));
        StartCoroutine(Play(source, mtv2, 12));
    }

    IEnumerator CoreMotive2(AudioSource source, AudioSource sourceH, AudioSource sourceB, float wait)
    {
        yield return new WaitForSeconds(wait);
        StartCoroutine(Play(source, mtv2, 0));
        StartCoroutine(Play(sourceH, mtv2h, 0));
        StartCoroutine(Play(sourceB, mtv2b, 0));

        float rand = Random.Range(0f, 100f) / 100;

        Debug.Log(rand);
        if (rand > 0.5f)
        {
            StartCoroutine(CleanSource(source, 24));
            StartCoroutine(CleanSource(sourceH, 24));
            StartCoroutine(CleanSource(sourceB, 24));
            Invoke("PlayRandomMotive", 0);
        }
        else
        {
            StartCoroutine(CoreMotive2(source, sourceH, sourceB, 12));
        }
    }

    void PlayRandomMotive()
    {
        int rand = Random.Range(0, 2);
        switch (rand)
        {
            case 0:
                PlayMotive1();
                break;
            case 1:
                PlayMotive2();
                break;
            default:
                Invoke("PlayRandomMotive", 6);
                break;

        }
    }

    IEnumerator Play(AudioSource source, AudioClip clip, float wait)
    {
        yield return new WaitForSeconds(wait);
        source.clip = clip;
        source.Play();
    }

    IEnumerator CleanSource(AudioSource source, float wait)
    {
        yield return new WaitForSeconds(wait);
        source.Pause();
        source.clip = null;
    }
}
